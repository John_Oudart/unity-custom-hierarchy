﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using System.IO;

public class CustomHierarchySettings: ScriptableObject
{
    public const string k_CustomHierarchySettingsPath = "Assets/Editor/CustomHierarchy/CustomHierarchySettings.asset";

    [SerializeField]
    public bool enableCustomHierarchy;
    
    [SerializeField]
    public bool enableTreeDrawing = true;

    [SerializeField]
    public bool enableSplitHighlight;

    [SerializeField]
    public Color splitColor;

    internal static CustomHierarchySettings GetOrCreateSettings()
    {
        var settings = AssetDatabase.LoadAssetAtPath<CustomHierarchySettings>(k_CustomHierarchySettingsPath);
        if (settings == null)
        {
            settings = ScriptableObject.CreateInstance<CustomHierarchySettings>();
            settings.enableCustomHierarchy = true;
            settings.enableTreeDrawing = true;
            settings.enableSplitHighlight = true;
            settings.splitColor = new Color(1f,1f,1f,0.8f);
            AssetDatabase.CreateAsset(settings, k_CustomHierarchySettingsPath);
            AssetDatabase.SaveAssets();
        }
        return settings;
    }

    internal static SerializedObject GetSerializedSettings()
    {
        return new SerializedObject(GetOrCreateSettings());
    }

}

//     // Register a SettingsProvider using IMGUI for the drawing framework:
// static class CustomHierarchySettingsIMGUIRegister
// {
//     [SettingsProvider]
//     public static SettingsProvider CreateCustomHierarchySettingsProvider()
//     {
//         // First parameter is the path in the Settings window.
//         // Second parameter is the scope of this setting: it only appears in the Project Settings window.
//         var provider = new SettingsProvider("Project/MyCustomIMGUISettings", SettingsScope.User)
//         {
//             // By default the last token of the path is used as display name if no label is provided.
//             label = "Custom IMGUI",
//             // Create the SettingsProvider and initialize its drawing (IMGUI) function in place:
//             guiHandler = (searchContext) =>
//             {
//                 var settings = CustomHierarchySettings.GetSerializedSettings();
//                 EditorGUILayout.PropertyField(settings.FindProperty("enableCustomHierarchy"), new GUIContent("enable Custom Hierarchy"));
//                 EditorGUILayout.PropertyField(settings.FindProperty("enableTreeDrawing"), new GUIContent("enable Tree Drawing"));
//                 EditorGUILayout.PropertyField(settings.FindProperty("enableSplitHighlight"), new GUIContent("enable Split Highlight"));
//                 EditorGUILayout.PropertyField(settings.FindProperty("splitColor"), new GUIContent("split Background Color"));
//             },

//             // Populate the search keywords to enable smart search filtering and label highlighting:
//             keywords = new HashSet<string>(new[] { "Number", "Some String" })
//         };

//         return provider;
//     }
// }



// Create CustomHierarchySettingsProvider by deriving from SettingsProvider:
// class CustomHierarchySettingsProvider : SettingsProvider
// {
//     private SerializedObject m_CustomSettings;

//     class Styles
//     {
//         public static GUIContent boolEnableCustomHierarchy = new GUIContent("enable Custom Hierarchy");
//         public static GUIContent boolEnableTreeDrawing = new GUIContent("enable Tree Drawing");
//         public static GUIContent boolEnableSplitHighlight = new GUIContent("enable Split Highlight");
//         public static GUIContent colorSplitColor = new GUIContent("split Background Color");
//     }

//     const string k_CustomHierarchySettingsPath = "Assets/Editor/CustomHierarchy/CustomHierarchySettings.asset";
//     public CustomHierarchySettingsProvider(string path, SettingsScope scope = SettingsScope.User)
//         : base(path, scope) {}

//     public static bool IsSettingsAvailable()
//     {
//         return File.Exists(k_CustomHierarchySettingsPath);
//     }

//     public override void OnActivate(string searchContext, VisualElement rootElement)
//     {
//         // This function is called when the user clicks on the MyCustom element in the Settings window.
//         m_CustomSettings = CustomHierarchySettings.GetSerializedSettings();
//     }

//     public override void OnGUI(string searchContext)
//     {
//         // Use IMGUI to display UI:
//         EditorGUILayout.PropertyField(m_CustomSettings.FindProperty("enableCustomHierarchy"), Styles.boolEnableCustomHierarchy);
//         EditorGUILayout.PropertyField(m_CustomSettings.FindProperty("enableTreeDrawing"), Styles.boolEnableTreeDrawing);
//         EditorGUILayout.PropertyField(m_CustomSettings.FindProperty("enableSplitHighlight"), Styles.boolEnableSplitHighlight);
//         EditorGUILayout.PropertyField(m_CustomSettings.FindProperty("splitColor"), Styles.colorSplitColor);
//     }

//     // Register the SettingsProvider
//     [SettingsProvider]
//     public static SettingsProvider CreateCustomHierarchySettingsProvider()
//     {
//         if (IsSettingsAvailable())
//         {
//             var provider = new CustomHierarchySettingsProvider("Project/CustomHierarchySettingsProvider", SettingsScope.Project);

//             // Automatically extract all keywords from the Styles.
//             provider.keywords = GetSearchKeywordsFromGUIContentProperties<Styles>();
//             return provider;
//         }

//         // Settings Asset doesn't exist yet; no need to display anything in the Settings window.
//         return null;
//     }
// }