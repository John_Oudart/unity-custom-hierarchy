﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]

public class CustomHierarchy
{
    static CustomHierarchy() {
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
        //Debug.Log("test");
    }

    static void HierarchyWindowItemOnGUI (int instanceID, Rect selectionRect) {
            GameObject gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

            CustomHierarchySettings customSettings = getSettings();
            if (customSettings.enableCustomHierarchy)
            {
                if (customSettings.enableTreeDrawing)
                    DrawTreeSymbol(selectionRect, gameObject);
                if (customSettings.enableSplitHighlight)
                    HighlightSplit(selectionRect, gameObject);
            }
    }

    static CustomHierarchySettings getSettings()
    {
        return AssetDatabase.LoadAssetAtPath<CustomHierarchySettings>("Assets/Editor/CustomHierarchy/CustomHierarchySettings.asset");
    }

    static void HighlightSplit (Rect selectionRect, GameObject gameObject)
    {
        CustomHierarchySettings customSettings = getSettings();
        Color splitColor = customSettings.splitColor;
        // Color "SPLIT" objects 
        if ( gameObject != null && gameObject.CompareTag("EditorOnly") && gameObject.name.Substring(0, 1) == ">" )
        {
            // Define rectangle will full width to be colored 
            Rect splitRect = new Rect(selectionRect.x-100, selectionRect.y, selectionRect.width*2, selectionRect.height);
            // Draw the actual rectangle with color
            EditorGUI.DrawRect(splitRect, splitColor);

        }
    }

    static void DrawTreeSymbol (Rect selectionRect, GameObject gameObject)
    {
        GUIStyle guiStyle = new GUIStyle();
        CustomHierarchySettings customSettings = getSettings();

        //Debug.Log(gameObject);
        Rect r = new Rect(selectionRect);
        // └  ├  ─  ┴  ┼  │
        string childSymbol = "├";
        string lastChildSymbol = "└";
        
        guiStyle.fontSize = 14;
        guiStyle.normal.textColor = new Color(0.5f, 0.5f, 0.5f, 1f);
        guiStyle.normal.textColor = new Color(0, 0, 0, 1f);




        if (gameObject != null)
        {
            // get number of parent (i.e. depth of the object)
            int parentCount = getParentCount(gameObject)+1;

            string leftSymbols = "";

            for (int i = 0; i < parentCount; i++)
            {
                leftSymbols=leftSymbols+"│";
            } 

            Rect symbolRect = new Rect(selectionRect.x -13*(parentCount+1)-3-parentCount, selectionRect.y - 3, selectionRect.width, selectionRect.height);
            Rect symbolRectUpShifted = new Rect(selectionRect.x -13*(parentCount+1)-3-parentCount, selectionRect.y, selectionRect.width, selectionRect.height);


            if ( gameObject.transform.childCount == 0)
            {

                if (gameObject.transform.parent != null)
                {
                    int siblingCount = gameObject.transform.parent.gameObject.transform.childCount;
                        int siblingIndex = gameObject.transform.GetSiblingIndex();

                    if (siblingIndex == siblingCount-1)
                    {
                        EditorGUI.LabelField(symbolRectUpShifted, leftSymbols+"│", guiStyle);
                        EditorGUI.LabelField(symbolRect,  leftSymbols+lastChildSymbol, guiStyle);
                    }
                    else
                    {
                        EditorGUI.LabelField(symbolRectUpShifted, leftSymbols+"│", guiStyle);
                        EditorGUI.LabelField(symbolRect,  leftSymbols+childSymbol, guiStyle);
                    }
                }
                else
                {
                    leftSymbols=leftSymbols+"│";
                    EditorGUI.LabelField(symbolRectUpShifted, leftSymbols.Substring(0,leftSymbols.Length-1)+"│", guiStyle);
                    EditorGUI.LabelField(symbolRect,  leftSymbols, guiStyle);
                }
            }
            else
            {
                EditorGUI.LabelField(symbolRectUpShifted, leftSymbols.Substring(0,leftSymbols.Length-1)+"│", guiStyle);
                EditorGUI.LabelField(symbolRect,  leftSymbols.Substring(0,leftSymbols.Length-1)+childSymbol, guiStyle);
            }
        }
    }

    static int getParentCount (GameObject gameObject)
    {
        // initialize parent count
        int parentCount=0;
        
        // if parent exists
        if (gameObject.transform.parent != null)
        {
            parentCount++;
            // recursive call to get parent count of parent until the top lvl hierarchy 
            parentCount += getParentCount(gameObject.transform.parent.gameObject);
        }
        return parentCount;
    }

}
